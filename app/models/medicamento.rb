class Medicamento < ApplicationRecord

	# SCOPES
	scope :vencidos, ->	{ where('validade <= ?', Date.current) }
	scope :disponiveis, ->	{ where('validade >= ? AND estoque > ?', Date.current, 0) }


	# CALLBACKS
	before_save :bota_tudo_em_maiusculo

	

	private
	
		def bota_tudo_em_maiusculo
			self.nome = nome.upcase
		end

end
