Rails.application.routes.draw do
  
	resources :medicamentos do 
	    member do
	      get :aumentar_estoque
	      patch :grava_aumento_estoque
	    end

	    collection do
	      get :vencidos
	      get :disponiveis
	    end
	end

end
