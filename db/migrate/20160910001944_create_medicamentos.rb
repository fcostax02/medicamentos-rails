class CreateMedicamentos < ActiveRecord::Migration[5.0]
  def change
    create_table :medicamentos do |t|
      t.string :nome
      t.integer :estoque
      t.date :validade

      t.timestamps
    end
  end
end
